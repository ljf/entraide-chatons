const softwares_uri = {
    'Jitsi': 'https://jitsi.org/',
    'CodiMD': 'https://demo.codimd.org/',
    'Etherpad': 'https://etherpad.org/',
    'Ethercalc': 'https://ethercalc.org/',
    'Framadate': 'https://framagit.org/framasoft/framadate',
    'Lufi': 'https://framagit.org/luc/lufi',
    'Lutim': 'https://github.com/ldidry/lutim',
    'LSTU': 'https://github.com/ldidry/lstu',
};
export default softwares_uri;
