import { useIntl } from "gatsby-plugin-intl"
import Layout from "../components/layout"
import SEO from "../components/seo"
import {graphql} from "gatsby";
import 'bootstrap/dist/css/bootstrap.min.css';
import "../styles/global.scss";
import {
  Col,
  Row,
} from "react-bootstrap"
import React, {useState} from "react"
import { ServiceCard } from '../components/service_card';

export default ({
  data: {
    dataJson: { nodes },
  },
}) => {
  //TODO link with the true json data
  //TODO check a11y
  //TODO finalize i18n IN PROGRESS

  const services = nodes.reduce((acc, { node }) => {
    node.category = node.Type_de_service.split(" (")[0]
    node.URL_site = 'https://chatons.org';
    const servicesExistants = acc[node.category] || []
    servicesExistants.push(node)
    acc[node.category] = servicesExistants
    return acc
  }, {})

  const randomInstance = instances => {
    const instances2 = instances.map(instance => [
      instance.Nom_du_chaton,
      parseInt(instance.Poids_dans_la_pani_re, 10) || 1,
    ])
    const keyFound = weightedRand(instances2)
    return instances.find(instance => instance.Nom_du_chaton === keyFound)
  }

  const weightedRand = data => {
    // First, we loop the main dataset to count up the total weight.
    // We're starting the counter at one because the upper boundary of Math.random() is exclusive.
    let total = 1
    for (let i = 0; i < data.length; i += 1) {
      total += data[i][1]
    }
    // Total in hand, we can now pick a random value akin to our
    // random index from before.
    const threshold = Math.floor(Math.random() * total)

    // Now we just need to loop through the main data one more time
    // until we discover which value would live within this
    // particular threshold. We need to keep a running count of
    // weights as we go, so let's just reuse the "total" variable
    // since it was already declared.
    total = 0
    for (let i = 0; i < data.length; i += 1) {
      // Add the weight to our running total.
      total += data[i][1]

      // If this value falls within the threshold, we're done!
      if (total >= threshold) {
        return data[i][0]
      }
    }
    // Should never happen
    return data[0][0]
  }


  const initialInstanceForService = Object.entries(services).reduce(
      (acc, [serviceName, services]) => {
        acc[serviceName] = randomInstance(services)
        return acc
      },
      {}
  )

  const [instanceForService, setInstanceForService] = useState(
      initialInstanceForService
  )

  const anotherInstance = service => {
    let newInstance = instanceForService[service]
      if (services[service].length === 1) return newInstance;
    while (
        newInstance.Nom_du_chaton === instanceForService[service].Nom_du_chaton
        ) {
      newInstance = randomInstance(services[service])
    }
    return newInstance
  }

  const changeInstanceForService = service => {
    setInstanceForService({
      ...instanceForService,
      [service]: anotherInstance(service),
    })
  }


    const intl = useIntl()
    //TODO create a component ServiceCard
    return (
        <Layout>
            <SEO
                lang={intl.locale}
                title={intl.formatMessage({ id: "home.title" })}
                keywords={[
                `chatons`,
                `chaton`,
                `logiciel`,
                `libres`,
                `service`,
                `collectif`,
                `hebergeur`,
                `alternatif`,
                `transparent`,
                `ouvert`,
                `neutre`,
                `solidaire`,
                ]}
            />
            <Row className="justify-content-md-center">
                {(
                    Object.entries(services).map(([key]) => (
                        <Col key={key} sm={"12"} md={"6"} lg={"6"} xl={"4"}>
                          <ServiceCard services={services} instanceForService={instanceForService} serviceKey={key} changeInstanceForService={changeInstanceForService} />
                        </Col>
                    ))
                )}
      </Row>
    </Layout>
  )
}

export const query = graphql`
  query {
    dataJson {
      nodes {
        node {
          Logiciels
          Nom_du_chaton
          Ouverture
          Type_de_service
          Poids_dans_la_pani_re
          URL_du_service
        }
      }
    }
  }
`
