import React from "react"
import { injectIntl } from "gatsby-plugin-intl"

const Footer = ({ intl }) => (
    <footer className="main-footer text-center text-black-50 mt-4 mb-4">
        {intl.formatMessage({ id: "footer.madeBy" })} <a href="https://chatons.org">CHATONS</a>. {intl.formatMessage({ id: "footer.illustrationsBy" })} <a href="http://www.peppercarrot.com/">David Revoy</a>. {intl.formatMessage({ id: "footer.contentsUnderLicence" })} <a href="https://creativecommons.org/licenses/by-sa/3.0/fr/">CC BY SA</a>. <a href="https://framagit.org/ljf/entraide-chatons">{intl.formatMessage({ id: "footer.sourceCode" })}</a>
    </footer>
)

export default injectIntl(Footer)
