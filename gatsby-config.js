/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

module.exports = {
    siteMetadata: {
        title: `Services CHATONS`,
        description: `Un sélecteur aléatoire de services libres pour décentraliser et découvrir un web à l'échelle humaine, solidaire et respectueux de votre vie privée !`,
        author: `@chatons`,
    },
    pathPrefix: `/entraide-chatons`,
    plugins: [
        {
            resolve: `gatsby-plugin-intl`,
            options: {
                // language JSON resource path
                path: `${__dirname}/src/intl`,
                // supported language
                languages: [`en`, `fr`, `de`],
                // language file path
                defaultLanguage: `fr`,
                // option to redirect to `/ko` when connecting `/`
                redirect: true,
            },
        },
        `gatsby-plugin-sass`,
        `gatsby-transformer-json`,
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                path: `./src/data/`,
            },
        },
    ],
}
